#include "mbed.h"
#include "TextLCD.h"

//LCD Display
TextLCD lcd(D11,D10,D9,D5,D4,D3,D2);

//Potmeter for temperatur simulering
AnalogIn pot(A1);
BusOut pot2(A0,A2);
double temp=0;

//Servomotor for luke
PwmOut servo(A5);
DigitalOut Laas(D7);

//Killpulse
PwmOut killpulse(PB_15);

//Manuell knapp
InterruptIn btn(USER_BUTTON);
Timeout TimeoutIrq;

//Ultralyd Sensor
InterruptIn ECHO(A4);
DigitalOut TRIG(A3);
Timer timer;
Ticker pulser;

//Tracker når det bør tømmes
static int antkill=0;
static int uptime=0;
Ticker updater1;
Ticker dogntemper;
static int dogntemp = 0;
static int dognteller = 0;

//Sender update til server
Ticker updater10;

//Killmode og parameter
BusIn mode(PC_8, PC_6);
int adjlength = 0;
int adjduty = 0;
int adjdelay =0;
int adjfrec = 0;

//Funksjons prototyper
void lcdtemp(void);
void trykk(void);
void enabletrykk(void);
void openclose(void);
void start(void);
void stopp(void);
void kill(void);
void update10(void);
void update1(void);
void pulse(void);
void dogntemps(void);


int main()
{
    pot2=1; //A2 =1, A0=0, ref verdier for potmeter
    Laas=1; //Magnet paa luke

    servo.period(0.010);                //periodetid pa servo er 10 ms
    servo.pulsewidth_us(1000);          //servo starter i lukket posisjon
    killpulse.period(0.010);            //periodetid pa killpuls
    killpulse.pulsewidth_ms(0);         //killpuls starter med 0% dutycycle

    updater1.attach(&update1,1);        //oppdaterer temp hver 1s
    updater10.attach(&update10,10);     //sender oppdatering over RS232 hver 10s
    pulser.attach(&pulse,0.1);          //sjekker etter bevegelse hver 100ms
    dogntemper.attach(&dogntemps,900);  //oppdaterer dogntemp hver 15.min

    btn.fall(&trykk);                   //manuell knapp for a registrere bevegelse
    ECHO.rise(&start);                  //start pa ekko til ultralydsensor
    ECHO.fall(&stopp);                  //slutt pa ekko til ultralydsensor

    lcd.gotoxy(10,1);
    lcd.printf("Kill:%2d", antkill);    //printer ant kill pa LCD

    lcd.gotoxy(1,2);
    lcd.printf("RatKill2000         "); //Velkommen melding til LCD

    char readstring[4];                 //Input string med adjustableparametre

    while(1) {

        scanf("%s", readstring);        //Leser inn fra RS232
        adjlength = readstring[0] - '0';//konverterer fra char til int
        adjduty = readstring[1] - '0';
        adjdelay = readstring[2] - '0';
        adjfrec = readstring[3] - '0';
        wait_ms(100);

    }
}

void lcdtemp(void)                      //oppdaterer live-temp til LCD
{
    if(temp<-9) {
        lcd.gotoxy(1,1);
        lcd.printf("Deg:%.0f  ", temp);
    } else if(temp<0) {
        lcd.gotoxy(1,1);
        lcd.printf("Deg: %.0f  ", temp);
    } else if(temp<10) {
        lcd.gotoxy(1,1);
        lcd.printf("Deg: +%.0f  ", temp);
    } else {
        lcd.gotoxy(1,1);
        lcd.printf("Deg:+%.0f  ", temp);
    }
}

void trykk(void)                        //starter ved detektert bevegelse
{
    btn.disable_irq();                  //slar av interrupt
    ECHO.disable_irq();
    TimeoutIrq.attach(&enabletrykk,30); //slar pa interrupt igjen etter 30 sek
    kill();                             //starter coilfunksjon
    openclose();                        //starter apne og lukke funksjon
    antkill+=1;                         //teller opp global variabel antkill
    lcd.gotoxy(10,1);
    lcd.printf("Kill:%2d", antkill);    //oppdaterer LCD med antkill
}


void enabletrykk(void)                  //slar pa interrupt
{
    btn.enable_irq();
    ECHO.enable_irq();
}

void openclose(void)                    //apne og lukke funksjon
{
    Laas=0;                             //slar av magnet
    wait_ms(50);
    for (int i=0; i<1000; i++) {        //lukker opp luke gradvis
        servo.pulsewidth_us(1000+i);
        wait_ms(1);
    }
    for (int i=0; i<1000; i++) {        //lukker igjen luke gradvis
        servo.pulsewidth_us(2000-i);
        wait_ms(1);
    }
    wait_ms(50);
    Laas=1;                             //slar pa magnet
}

void start(void)                        //starter timer ved deteksjon
{
    timer.reset();
    timer.start();
}

void stopp(void)                        //slutter timer ved deteksjon
{
    static double tidUs;
    timer.stop();                       //leser timer

    tidUs = timer.read_us();

    if((tidUs>100) && (tidUs<800)) {    //ved rett avstand starter deteksjonsfunksjon
        trykk();
    }
}

void kill(void)                         //coilfunksjon
{
    int m=mode;                         //leser mode fra GPIO pinner
    int topp =0, bunn=0;
    switch(m) {
        case 0:                         //killmode
            topp=10;
            bunn=2;
            adjlength=5;
            adjdelay=0;
            adjfrec=1;
            break;
        case 1:                         //stunmode
            topp=2;
            bunn=0;
            adjlength=5;
            adjdelay=0;
            adjfrec=1;
            break;
        case 2:                         //adjustablemode
            topp=adjduty;
            bunn=0;
            break;
    }
    wait(adjdelay);                     //kan vente hvis angitt i adjustable
    for(int i=0; i<(adjlength*adjfrec); i++) {      //lokke kjorer 5 s, eller avhegig av adj
        killpulse.pulsewidth_ms(topp);              //dutycycle angitt av variabler
        wait((float)((1.0f/(float)(adjfrec))/2.0f));//0.5 s wait, eller angitt av adj
        killpulse.pulsewidth_ms(bunn);
        wait((float)((1.0f/(float)(adjfrec))/2.0f));
    }
    killpulse.pulsewidth_ms(0);         //slar av coil
}

void update10(void)                     //sender oppdatering over RS232 hvert 10 s
{
    int m=mode;
    char killmode[4][20]= {"Kill","Stun","Adjustable", "Error"};
    if((dogntemp>30)&&(antkill>0)) {    //dogntemp over 30 grader og mer enn 1 kill
        lcd.gotoxy(1,2);
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, Warning, Please Empty");
    } else if((dogntemp>15)&&(antkill>0)&&(uptime>86400)&&(mode==0)) {
        lcd.gotoxy(1,2);                //dogntemp over 15, mer enn 1 kill og 1 dag pa
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, Warning, Please Empty");
    } else if((dogntemp>0)&&(antkill>0)&&(uptime>259200)&&(mode==0)) {
        lcd.gotoxy(1,2);                //dogntemp over 0, mer enn 1 kill og 3 dager pa
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, Warning, Please Empty");
    } else if((antkill>0)&&(uptime>604800)&&(mode==0)) {
        lcd.gotoxy(1,2);                //mer enn 1 kill og 1 uke pa
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, , Warning, Please Empty");
    } else if((antkill>0)&&(mode!=0)) { //mer enn 1 kill og ikke i killmode
        lcd.gotoxy(1,2);
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, Warning,  Please Empty");
    } else if(antkill>3) {              //mer enn 3 kill
        lcd.gotoxy(1,2);
        lcd.printf("Please Empty    ");
        printf("\r\n$Status, Warning,  Please Empty");
    } else {
        lcd.gotoxy(1,2);
        lcd.printf("Mode: %s        ", killmode[m]);
    }
    printf("\r\n$Mode, %s", killmode[m]);//mode sendt over RS232
    printf("\r\n$Temp, %.2f",temp);     //temp sendt over RS232
    printf("\r\n$Uptime, %d", uptime);  //uptime sendt over RS232
    printf("\r\n$Kill, %d", antkill);   //antkill sendt over RS232

    if(m>2)                             //ved annen mode enn 0,1,2
        printf("\r\n$Status, Error, Mode");

    if(uptime<0)                        //ved negativ oppetid
        printf("\r\n$Status, Error, Uptime");

}

void update1(void)                      //oppdatering hver 1 s
{
    uptime++;                           //oppdaterer oppetid
    temp=-40+(pot.read()*100);          //leser av temp
    lcdtemp();                          //kjorer LCDfunksjon
}

void pulse(void)                        //sender puls ved ultralydsensor hver 100ms
{
    TRIG=1;
    wait_ms(10);
    TRIG=0;
}

void dogntemps(void)                    //beregner dogntemp hver 15 min
{
    dognteller +=1;
    dogntemp +=temp;
    printf("\r\n$DT,%d",int(dogntemp/dognteller));  //dogntemp sendt over RS232
}
