from flask import Flask,send_file,request
app = Flask(__name__)

@app.route('/')
def SendImage():
    return send_file('test.jpg', mimetype='image/jpg')

if __name__ == '__main__':
    app.run(host='0.0.0.0') #everyone is allowed to access my Server
