﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public string strRecievedUDPMessage;                                                

        public void DoReciveUDP()                                                           //Motta UDP pakker
        {
            UdpClient sock = new UdpClient(9050);
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 0);

            while(true)
            {
                try
                {
                    byte[] data = sock.Receive(ref iep);
                    strRecievedUDPMessage = Encoding.ASCII.GetString(data, 0, data.Length);
                    this.Invoke(new EventHandler(this.UdpDataRecieved));
                }
                catch (Exception e) { }
            }
            sock.Close();
        }

        public void DoSendUDP0()                                                            //Send UDP pakker
        {
            UdpClient udpClient = new UdpClient("128.39.114.151", 9060);                    //IP:128.39.114.151 Port:9060
            Byte[] sendBytes = Encoding.ASCII.GetBytes("$Mode,0,0");                        //Sender "$Mode,0,0"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void DoSendUDP1()                                                
        {
            UdpClient udpClient = new UdpClient("128.39.114.151", 9060);                    //IP:128.39.114.151 Port:9060
            Byte[] sendBytes = Encoding.ASCII.GetBytes("$Mode,0,1");                        //Sender "$Mode,0,1"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void DoSendUDP2()
        {
            int length = trackBar1.Value;                                                   //Henter verdier fra trackbars
            int duty = trackBar2.Value;
            int delay = trackBar3.Value;
            int frec = trackBar4.Value;
            string Adjustable = length.ToString() + duty.ToString() + delay.ToString() + frec.ToString();   //Lagrer alle tackbarsverdier som en string
            UdpClient udpClient = new UdpClient("128.39.114.151", 9060);                    //IP:128.39.114.151 Port:9060
            Byte[] sendBytes = Encoding.ASCII.GetBytes("$Mode,1,0");                        //Sender "$Mode,1,0"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            System.Threading.Thread.Sleep(2000);                                            //venter 2 sekunder
            sendBytes = Encoding.ASCII.GetBytes(Adjustable);                                //Sender trackbarverdier
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Reset()     
        {
            UdpClient udpClient = new UdpClient("128.39.114.151", 9060);                    //IP:128.39.114.151 Port:9060
            Byte[] sendBytes = Encoding.ASCII.GetBytes("$Reset,0");                         //Sender "$Reset,0"
            
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            System.Threading.Thread.Sleep(2000);                                            //venter 2 sekunder
            sendBytes = Encoding.ASCII.GetBytes("$Reset,1");                                //Sender "$Reset,1"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Camera()
        {
            UdpClient udpClient = new UdpClient("128.39.114.151", 9060);                    //IP:128.39.114.151 Port:9060
            Byte[] sendBytes = Encoding.ASCII.GetBytes("$Camera,1");                        //Sender "$Camera,1"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            System.Threading.Thread.Sleep(2000);                                            //venter 2 sekunder
            sendBytes = Encoding.ASCII.GetBytes("$Camera,0");                               //Sender "$Camera,0"
            try
            {
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void UdpDataRecieved(object sender, EventArgs e)                             //Oppdaterer textboksverdier pa bakgrunn av mottatt UDP
        {
            string csv = strRecievedUDPMessage;
            string[] values;
            values = csv.Split(',');                                                        //Splitter CSV 
            if (values[0] == "$Mode")
                textBox2.Text = values[1];
            if (values[0] == "$Kill")
                textBox4.Text = values[1];
            if (values[0] == "$Temp")
                textBox3.Text = values[1];
            if (values[0] == "$Uptime")
                textBox7.Text = values[1];
            if (values[0] == "$DT")
                textBox20.Text = values[1];

            if (values[0] == "$Status")
            {
                if (values[1] == " Warning")
                {
                    textBox9.Text = values[2];
                    pictureBox2.Visible = false;
                    pictureBox1.Visible = true;
                }
                
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private Thread workerThread;
        private void Form1_Load(object sender, EventArgs e)
        {
            workerThread = new Thread(this.DoReciveUDP);
            workerThread.IsBackground = true;
            workerThread.Start();
        }

        private void button1_Click(object sender, EventArgs e)                              //Knapp Kill
        {
            DoSendUDP0();
        }

        private void button2_Click(object sender, EventArgs e)                              //Knapp Stun
        {
            DoSendUDP1();
        }

        private void button3_Click(object sender, EventArgs e)                              //Knapp Adjustable
        {
            DoSendUDP2();
        }

        private void button4_Click(object sender, EventArgs e)                              //Knapp Reset
        {
            Reset();
        }

        private void button5_Click(object sender, EventArgs e)                              //Knapp Take Picture
        {
            Camera();
            System.Threading.Thread.Sleep(10000);                                           //Vent 10 sekunder
            pictureBox3.ImageLocation = "http://128.39.114.151:5000/";                      //Oppdaterer bilde fra url
        }

        private void trackBar1_Scroll(object sender, EventArgs e)                           //Oppdaterer tekstboks ved endring pa trackbar
        {
            int verdi = trackBar1.Value;
            textBox15.Text=verdi.ToString();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)                           //Oppdaterer tekstboks ved endring pa trackbar
        {
            int verdi = trackBar2.Value*10;
            textBox16.Text = verdi.ToString();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)                           //Oppdaterer tekstboks ved endring pa trackbar
        {
            int verdi = trackBar3.Value;
            textBox17.Text = verdi.ToString();
        }

        private void trackBar4_Scroll(object sender, EventArgs e)                           //Oppdaterer tekstboks ved endring pa trackbar
        {
            int verdi = trackBar4.Value;
            textBox18.Text = verdi.ToString();
        }


        
        
    }
}
